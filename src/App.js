import React, { Component } from 'react';
import { FaAlignRight } from 'react-icons';
import logo from './logo.svg';
import './App.css';

const nav = [
            {
                link: "/",
                text:"Home"
            },
            {
                link: "/login/",
                text:"Login"
            },
            {
                link: "/about/",
                text:"About us"
            },
            {
                link: "/contact/",
                text:"Contact us"
            }
 ];
