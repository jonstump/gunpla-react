Gunpla Library Collection App
============
[![GitHub Stars](https://img.shields.io/github/stars/jonstump/XXX.svg)](https://github.com/jonstump/XXX/stargazers) [![GitHub Issues](https://img.shields.io/github/issues/jonstump/XXX.svg)](https://github.com/jonstump/XXX/issues) [![Live Demo](https://img.shields.io/badge/demo-online-green.svg)](https://jonstump.com/XXX)

This project is currently a WIP. What it will eventually be is a react web app that allows users to track and review their favorite gunpla models that they have built. It also will have a library feature so that users can keep track of what they have built and reviewed.

---
## Tech Used
- JavaScript
- React
- Neovim

---
## Buy me a coffee

Whether you use this project, have learned something from it, or just like it, please consider supporting it by buying me a coffee :)

<a href="https://www.buymeacoffee.com/jonstump" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: auto !important;width: auto !important;" ></a>

---

## Features to Implement
- Main landing page that show top rated gunpla
- Individual Gunpla pages that include a photo library and reviews
- Ability to add user account
- Review Page
- CRUD for User library of Gunpla models
- Other awesome features yet to be implemented

---

## Setup
Clone this repo to your desktop and run `npm install` to install all the dependencies.

Once the dependencies are installed, you can run  `npm start` to start the application. You will then be able to access it at localhost:3000

---

## Usage


---

## License
>You can check out the full license [here]()


